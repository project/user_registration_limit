User Registration Limit
------------
This module allows a site to limit user registrations based on the configurations like, only 50 number of user registrations are allowed for the site.

#### Features covered
1. Admins can configure the User Registration limit, This limit can be increased or decreased.
2. Admins can see a warning message on Account Settings page, if limit is reached.
3. Users cam see a warning message & access denied on User Registration page, if limit is reached.

INSTALLATION
------------
* Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

CONFIGURATION
------------
* After enabling the module, navigate to Account Settings `/admin/config/people/accounts`, you can set the **User Registration limit** and Click on **Save Configuration**.
* Make sure you enter the User Registration limit more than the existing users in your site.
* Navigate to User Registration Limit Settings `admin/config/user-registration-limit`, you can add the **User registrations warning message** and Click on **Save Configuration**.
* This is all on the configuration.
  * If User Registration limit is reached
    * Admin will see a warning message on Account Settings page.
    * Users will see a warning message & access denied on User Registration page.

