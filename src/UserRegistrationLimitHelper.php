<?php

namespace Drupal\user_registration_limit;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * A Drupal service with business logic for user registration limits.
 */
class UserRegistrationLimitHelper {
  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * The name of the configuration object.
   */
  const CONFIG_NAME = 'user_registration_limit';

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The user setting config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $userSettingsConfig;

  /**
   * Current logged in drupal user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a RegistrationAccessCheck object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountInterface $current_user, EntityTypeManagerInterface $entity_type_manager) {
    $this->configFactory = $config_factory;
    $this->userSettingsConfig = $config_factory->get('user.settings');
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Gets the current user limit.
   */
  public function getUserLimit() {
    return $this->userSettingsConfig->get(self::CONFIG_NAME);
  }

  /**
   * Gets the current user limit.
   *
   * @param int|null $value
   *   The value to save to config.
   */
  public function setUserLimit(int|NULL $value) {
    $config = $this->configFactory->getEditable('user.settings');
    $config->set(self::CONFIG_NAME, $value)->save(TRUE);
    return $this->userSettingsConfig->get(self::CONFIG_NAME);
  }

  /**
   * Check if the current user can registrater a user.
   *
   * @return bool
   *   Returns true if a new user can register.
   */
  public function canUserRegister(): bool {
    // In case if user is having has 'administer-users' permission,
    // that user can create any number of users.
    if ($this->currentUser->hasPermission('administer users')) {
      return TRUE;
    }
    return !$this->isUsersRegistersAtLimit();
  }

  /**
   * Check the user registrations limit.
   *
   * @return bool
   *   Returns true if the number of current users are under the set limit.
   */
  public function isUsersRegistersAtLimit(): bool {
    $userRegistrationLimit = (int) ($this->userSettingsConfig->get(self::CONFIG_NAME) ?? 0);
    if (empty($userRegistrationLimit)) {
      return FALSE;
    }
    $numberOfUsers = $this->numberOfUsers();
    return $userRegistrationLimit <= $numberOfUsers;
  }

  /**
   * Checks if the number of user limit is valid.
   *
   * @param int|null $limit
   *   The value of the limit to validated.
   *
   * @return null|string|MarkupInterface
   *   Returns a (string) message if the limit is not valid.
   */
  public function validateProposedUserLimit(int|NULL $limit): string|MarkupInterface|NULL {
    $numberOfUsers = $this->numberOfUsers();
    if (empty($limit) || $numberOfUsers <= $limit) {
      return NULL;
    }
    return t('Always User Registration limit value should be more than users existing in site.');
  }

  /**
   * Creates a query to get users to which to count.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   A query interface that gets users.
   */
  protected function userQuery(): QueryInterface {
    return $this->entityTypeManager->getStorage('user')->getQuery();
  }

  /**
   * Uses the user query to get the number of users.
   */
  protected function numberOfUsers(): int {
    return $this->userQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();
  }

  /**
   * Gets message from config when user cannot register.
   *
   * @return string|MarkupInterface|null
   *   The message.
   */
  public function getCannotRegisterMessage(): string|MarkupInterface|NULL {
    $config = $this->configFactory->get('user_registration_limit.settings');
    return $config->get('warning_message');
  }

}
