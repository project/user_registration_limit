<?php

namespace Drupal\user_registration_limit\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implement class UserRegistrationLimitSettings.
 */
class UserRegistrationLimitSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'user_registration_limit.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_registration_limit_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('user_registration_limit.settings');

    $form['warning_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('User registrations warning message'),
      '#description' => $this->t('This is the warning message which will be shown to the users,
        once user registration limit set by admin is been reached the limit.'),
      '#default_value' => $config->get('warning_message'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('user_registration_limit.settings')
      ->set('warning_message', $form_state->getValue('warning_message'))
      ->save();
  }

}
